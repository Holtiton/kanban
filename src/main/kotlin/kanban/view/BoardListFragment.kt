package kanban.view

import javafx.beans.property.SimpleStringProperty
import javafx.scene.text.Font

import kanban.controller.KanbanController
import kanban.model.Board
import kanban.model.BoardModel

import tornadofx.*

class BoardListFragment : ListCellFragment<Board>() {
    private val board = BoardModel().bindTo(this)
    private var boardDescription = text(board.description)
    private val kanban: KanbanController by inject()
    private val boardTicketCount = SimpleStringProperty("Ticket count")
    init {
        // The cellProperty is not always set and I couldn't figure out how to get the width set
        // so the wrapping would work so I added a listener to it that binds it whenever it is available
        cellProperty.addListener {
            observable, _, _ ->
            if (observable.value != null) {
                boardDescription.wrappingWidthProperty().bind(observable.value?.listView?.widthProperty()?.add(-100))
                boardTicketCount.set("Tickets: ${kanban.countOpenTicket(board.title.value)}")
            }
        }
    }

    override val root = vbox {
        text(board.title) {
            font = Font(18.0)
        }
        hbox {
            add(boardDescription)
            spacer()
            label(boardTicketCount)
//            label("Tickets: ${kanban.countOpenTicket(board.title.value)}")
        }
    }
}