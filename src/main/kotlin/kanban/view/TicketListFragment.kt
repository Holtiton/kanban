package kanban.view

import kanban.model.Ticket
import kanban.model.TicketModel
import tornadofx.*

class TicketListFragment : ListCellFragment<Ticket>() {
    private val ticket = TicketModel().bindTo(this)
    private var ticketText = text(ticket.title)

    init {
        // The cellProperty is not always set and I couldn't figure out how to get the width set
        // so the wrapping would work so I added a listener to it that binds it whenever it is available
        cellProperty.addListener {
            observable, _, _ ->
            if (observable.value != null) {
                ticketText.wrappingWidthProperty().bind(observable.value?.listView?.widthProperty()?.add(-40))
            }
        }
    }

    override val root = form {
        label(ticket.id)
        add(ticketText)
    }
}