package kanban.view


import javafx.application.Platform
import javafx.scene.input.KeyCode
import javafx.scene.text.Font
import javafx.stage.StageStyle
import kanban.controller.KanbanController
import kanban.model.BoardModel
import tornadofx.*

class BoardView : View("Personal Kanban") {
    private val kanban: KanbanController by inject()
    private val selectedBoard: BoardModel by inject()
    override val root = borderpane {
        top = borderpane {
            borderpaneConstraints { margin = insets(10) }
            left = vbox {
                text("Personal Kanban") {
                    font = Font(22.0)
                }
            }
            right = button("New board") {
                action {
                    val editMode = false
                    find<BoardEdit>(mapOf(BoardEdit::editMode to editMode)).openModal(stageStyle = StageStyle.DECORATED)
                }
                shortcut("N")
            }
        }
        center = stackpane {
            stackpaneConstraints { padding = insets(10) }
            listview(kanban.listBoards()) {
                cellFragment(BoardListFragment::class)
                selectionModel.selectedItemProperty().onChange {
                    board -> selectedBoard.item = board
                }
                Platform.runLater {
                    requestFocus()
                    scrollTo(0)
                    selectionModel.select(0)
                }
                contextmenu {
                    item("Edit").action {
                        val editMode = true
                        find<BoardEdit>(mapOf(BoardEdit::editMode to editMode)).openModal(stageStyle = StageStyle.DECORATED)
                    }
                }
                onDoubleClick {
                    replaceWith(TicketView::class, ViewTransition.Slide(0.3.seconds, ViewTransition.Direction.LEFT))
                }
                setOnKeyPressed {
                    event ->
                    if (event.code == KeyCode.ENTER) {
                        replaceWith(TicketView::class, ViewTransition.Slide(0.3.seconds, ViewTransition.Direction.LEFT))
                    }
                }
            }
        }
    }
}