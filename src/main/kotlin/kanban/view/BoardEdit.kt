package kanban.view

import javafx.beans.property.SimpleBooleanProperty
import javafx.scene.control.Alert
import javafx.scene.control.ButtonType
import kanban.controller.KanbanController
import kanban.model.Board
import kanban.model.BoardModel
import tornadofx.*

class BoardEdit : Fragment() {
    private val kanban: KanbanController by inject()
    private val selectedBoard: BoardModel by inject()
    var editMode = false
    var newTicket = Board("<new id>","","", listOf(""))

    init {
        val editModeParam = params["editMode"] as? Boolean
        if (editModeParam != null) {
            editMode = editModeParam
        }

        if (editMode) {
            this.title = "Edit board"
        } else {
            this.title = "Create board"
            selectedBoard.item = newTicket
        }
    }

    override val root = form {
        fieldset("Board details") {
            field("Id") {
                label(selectedBoard.id)
            }
            field("Title") {
                textfield(selectedBoard.title)
            }
            field("Description") {
                textarea(selectedBoard.description) {
                    prefRowCount = 5
                }
            }
        }
        buttonbar {
            button("Delete") {
                visibleWhen(SimpleBooleanProperty(editMode))
                action {
                    alert(Alert.AlertType.CONFIRMATION,
                            "Delete?",
                            "Are you sure you want to delete ${selectedBoard.title.value}",
                            ButtonType.YES, ButtonType.NO) {
                        buttonType ->
                        if (buttonType == ButtonType.YES) {
                            kanban.deleteBoard()
                            this@BoardEdit.close()
                        }
                    }
                }
            }
            button {
                text = if (editMode) {
                    "Save"
                } else {
                    "Create"
                }
                enableWhen(selectedBoard.dirty)
                action {
                    selectedBoard.commit()
                    if (!editMode) {
                        kanban.createBoard()
                    }
                    this@BoardEdit.close()
                }
            }
            button("Reset") {
                action {
                    selectedBoard.rollback()
                }
            }
            button("Cancel") {
                action {
                    selectedBoard.rollback()
                    this@BoardEdit.close()
                }
            }
        }
    }
}