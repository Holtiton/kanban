package kanban.view

import javafx.scene.control.ListView
import javafx.scene.control.SelectionMode
import javafx.scene.input.*
import javafx.scene.layout.BorderPane
import javafx.scene.layout.Priority
import javafx.scene.text.Font
import javafx.stage.StageStyle

import kanban.controller.KanbanController
import kanban.model.BoardModel
import kanban.model.Ticket
import kanban.model.TicketModel

import tornadofx.*

class TicketView : View("Personal Kanban") {
    val TICKET_STATE = DataFormat("taskstate")
    private val kanban: KanbanController by inject()
    private val selectedTicket : TicketModel by inject()
    private val selectedBoard: BoardModel by inject()

    private var ticketList = ListView<Ticket>()

    override val root = borderpane {
        top = header()
        center = ticketList()
        //bottom(Footer::class)
    }

    override fun onDock() {
        super.onDock()
        // reload new tickets
        root.center = root.ticketList()
    }

    override fun onUndock() {
        super.onUndock()
        root.center = borderpane {  }
    }

    private fun BorderPane.header() = borderpane {
        borderpaneConstraints { margin = insets(10) }
        left = button("Boards") {
            action {
                replaceWith(BoardView::class, ViewTransition.Slide(0.3.seconds, ViewTransition.Direction.RIGHT))
            }
            shortcut("B")
        }
        center = text(selectedBoard.title) {
            font = Font(20.0)
        }
        right = button("New ticket...") {
            useMaxWidth = true
            action {
                val editMode = false
                find<TicketEdit>(mapOf(TicketEdit::editMode to editMode)).openModal(stageStyle = StageStyle.DECORATED)
            }
            shortcut("N")
        }
    }

    private fun BorderPane.ticketList() = scrollpane(fitToHeight = true) {
        hbox {
            prefWidthProperty().bind(this@scrollpane.widthProperty())
            for (status: String in kanban.listTicketStates()) {
                vbox {
                    hboxConstraints { hGrow = Priority.ALWAYS }
                    spacing = 10.0
                    label(status)
                    ticketList = listview(kanban.listTickets(status = status)) {
                        hboxConstraints { hGrow = Priority.ALWAYS }
                        vboxConstraints { vGrow = Priority.ALWAYS }
                        cellFragment(TicketListFragment::class)
                        minWidth = 200.0

                        selectionModel.selectionMode = SelectionMode.SINGLE
                        selectionModel.selectedItemProperty().onChange {
                            ticket -> selectedTicket.item = ticket
                        }
                        contextmenu {
                            item("Edit").action {
                                val editMode = true
                                find<TicketEdit>(mapOf(TicketEdit::editMode to editMode)).openModal(stageStyle = StageStyle.DECORATED)
                            }
                        }
                        onDoubleClick {
                            val editMode = true
                            find<TicketEdit>(mapOf(TicketEdit::editMode to editMode)).openModal(stageStyle = StageStyle.DECORATED)
                        }
                        setOnDragDetected {
                            event -> dragDetected(event, this, status)
                        }
                        setOnDragOver {
                            event -> dragOver(event, this, status)
                        }
                        setOnDragDropped {
                            event -> dragDropped(event, status)
                        }
                    }
                }
                children.style {
                    padding = box(10.px)
                }
            }
        }
    }


    private fun dragDetected(e: MouseEvent, listView: ListView<Ticket>, status: String) {
        // this might be redundant but I was getting exceptions with not having
        // a ticket selected
        selectedTicket.item = listView.selectionModel.selectedItem
        val dragboard = listView.startDragAndDrop(TransferMode.MOVE)

//        val tempTicketFragment = TicketListFragment()
//        val snapshotParameters = SnapshotParameters()
//        snapshotParameters.fill = Color.TRANSPARENT
//        tempTicket.isManaged = false
//        Scene(tempTicket) // scene required for snapshot
//        dragboard.dragView = tempTicket.snapshot(snapshotParameters, null)

        val content = ClipboardContent()
        content.put(TICKET_STATE, status)
        dragboard.setContent(content)
        e.consume()
    }

    private fun dragOver(e: DragEvent, listView: ListView<Ticket>, status: String) {
        val dragboard = e.dragboard
        if (e.gestureSource != listView && dragboard.hasContent(TICKET_STATE)) {
            val fromStatus = dragboard.getContent(TICKET_STATE) as String
//            val allowedStatus = model.getCurrentProject().getWorkflow().get(fromStatus)
//            if (allowedStatus.contains(status)) {
            e.acceptTransferModes(TransferMode.MOVE)
//            }
        }
        e.consume()
    }

    private fun dragDropped(e: DragEvent, status: String) {
        var dragCompleted = false
        if (e.dragboard.hasContent(TICKET_STATE)) {
            kanban.updateTicketStatus(status = status)
            dragCompleted = true
        }
        e.isDropCompleted = dragCompleted
        e.consume()
    }
}