package kanban.view

import javafx.beans.property.SimpleBooleanProperty
import javafx.scene.control.Alert
import javafx.scene.control.ButtonType
import kanban.controller.KanbanController
import kanban.model.Ticket
import kanban.model.TicketModel

import tornadofx.*

class TicketEdit : Fragment() {
    private val kanban : KanbanController by inject()
    private val selectedTicket: TicketModel by inject()
    var editMode = false
    var newTicket = Ticket("<new id>","","", "")

    init {
        val editModeParam = params["editMode"] as? Boolean
        if (editModeParam != null) {
            editMode = editModeParam
        }

        if (editMode) {
            this.title = "Edit ticket"
        } else {
            this.title = "Create ticket"
            selectedTicket.item = newTicket
        }
    }

    override val root = form {
        fieldset("Ticket details") {
            field("Id") {
                label(selectedTicket.id)
            }
            field("Title") {
                textfield(selectedTicket.title)
            }
            field("Status") {
                label(selectedTicket.status)
            }
            field("Description") {
                textarea(selectedTicket.description) {
                    prefRowCount = 5
                }
            }
        }
        buttonbar {
            button("Delete") {
                visibleWhen(SimpleBooleanProperty(editMode))
                action {
                    alert(Alert.AlertType.CONFIRMATION,
                            "Delete?",
                            "Are you sure you want to delete ${selectedTicket.title.value}",
                            ButtonType.YES, ButtonType.NO) {
                        buttonType ->
                        if (buttonType == ButtonType.YES) {
                            kanban.deleteTicket()
                            this@TicketEdit.close()
                        }
                    }
                }
            }
            button {
                text = if (editMode) {
                    "Save"
                    } else {
                        "Create"
                    }
                enableWhen(selectedTicket.dirty)
                action {
                    selectedTicket.commit()
                    if (!editMode) {
                        kanban.createTicket()
                    }
                    this@TicketEdit.close()
                }
            }
            button("Reset") {
                action {
                    selectedTicket.rollback()
                }
            }
            button("Cancel") {
                action {
                    selectedTicket.rollback()
                    this@TicketEdit.close()
                }
            }
        }
    }
}