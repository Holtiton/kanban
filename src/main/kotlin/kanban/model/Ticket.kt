package kanban.model

import javafx.beans.property.SimpleStringProperty
import tornadofx.ItemViewModel
import tornadofx.*

class Ticket(id: String, title: String, description: String, status: String) {
    val idProperty = SimpleStringProperty(id)
    var id by idProperty

    val titleProperty = SimpleStringProperty(title)
    var title by titleProperty

    val descriptionProperty = SimpleStringProperty(description)
    var description by descriptionProperty

    val statusProperty = SimpleStringProperty(status)
    var status by statusProperty
}


class TicketModel : ItemViewModel<Ticket>() {
    val id = bind(Ticket::idProperty)
    val title = bind(Ticket::titleProperty)
    val description = bind(Ticket::descriptionProperty)
    val status = bind(Ticket::statusProperty)
}
