package kanban.model

import javafx.beans.property.SimpleListProperty
import javafx.beans.property.SimpleStringProperty
import tornadofx.ItemViewModel
import tornadofx.getValue
import tornadofx.observable
import tornadofx.setValue

class Board(id: String, title: String, description: String, ticketStates: List<String>) {
    val idProperty = SimpleStringProperty(id)
    var id by idProperty

    val titleProperty = SimpleStringProperty(title)
    var title by titleProperty

    val descriptionProperty = SimpleStringProperty(description)
    var description by descriptionProperty

    val ticketStatesProperty = SimpleListProperty<String>(ticketStates.observable())
    val ticketStates by ticketStatesProperty
}

class BoardModel : ItemViewModel<Board>() {
    val id = bind { item?.idProperty }
    val title = bind { item?.titleProperty }
    val description = bind { item?.descriptionProperty }
    val ticketStates = bind { item?.ticketStatesProperty }
}
