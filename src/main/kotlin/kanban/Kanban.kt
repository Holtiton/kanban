package kanban

import javafx.stage.Stage
import kanban.view.BoardView
import tornadofx.*

class Kanban: App() {
    override val primaryView = BoardView::class
    override fun start(stage: Stage) {
        super.start(stage)
        stage.minWidth = 800.0
        stage.minHeight = 600.0
    }
}

fun main(args: Array<String>) = launch<Kanban>(args)
