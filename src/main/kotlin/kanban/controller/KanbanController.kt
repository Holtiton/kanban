package kanban.controller

import javafx.collections.ObservableList
import kanban.model.Board
import kanban.model.BoardModel
import kanban.model.Ticket
import kanban.model.TicketModel
import tornadofx.Controller
import tornadofx.observable
import java.util.*

class KanbanController : Controller() {
    private val boardModel: BoardModel by inject()
    private val ticketModel : TicketModel by inject()

    var backlogTickets = arrayListOf(
            Ticket("1", "Test doing something longer, this was not enough, do also this", "Something", "Backlog"),
            Ticket("2", "Do this", "Something", "Backlog"),
            Ticket("3", "Do that", "Something", "Backlog")).observable()
    var todoTickets = arrayListOf(
            Ticket("4", "Do something larger", "Something", "Todo"),
            Ticket("5", "Do something else", "Something", "Todo"),
            Ticket("6", "Try thiss", "Something", "Todo")).observable()
    var doingTickets = arrayListOf(Ticket("7", "Check that", "Something", "Doing")).observable()
    var doneTickets = arrayListOf(Ticket("8", "Notify someone", "Something", "Done")).observable()

    var backlogTickets2 = arrayListOf(
            Ticket("1", "Test doing something longer, this was not enough, do also this", "Something", "Backlog"),
            Ticket("2", "Test3", "Something", "Backlog")).observable()
    var todoTickets2 = arrayListOf(
            Ticket("3", "Test doing something longer, this do also this", "Something", "Todo"),
            Ticket("4", "Test35", "Something", "Todo")).observable()
    var doingTickets2 = arrayListOf(Ticket("5", "Test4", "Something", "Doing")).observable()
    var doneTickets2 = arrayListOf(Ticket("6", "Test4", "Something", "Done")).observable()

    var ticketMap = HashMap<String, ObservableList<Ticket>>()

    var boards = arrayListOf<Board>().observable()

    init {
        boards.addAll(Board("1", "Operational Excellence", "does something", listOf("Backlog", "Todo", "Doing", "Done")),
                Board("2", "Maintenance", "does something else", listOf("Backlog", "Todo", "Doing", "Done")))

    }

    fun listBoards() : ObservableList<Board> {
        return boards
    }

    fun listTicketStates(projectName: String = boardModel.item.title) : List<String> {
        return boardModel.item.ticketStates
    }

    fun countOpenTicket(boardName: String = boardModel.item.title) : Int {
        val board = boards.filter {
            board ->
            board.title == boardName
        }

        var count = 0
        for (state: String in board[0].ticketStates) {
            count += listTickets(board[0].title, state).count()
        }
        return count
    }

    fun listTickets(boardName: String = boardModel.item.title, status: String) : ObservableList<Ticket> {
        if (boardName == "Operational Excellence") {
            ticketMap.clear()
            ticketMap.put("Backlog", backlogTickets)
            ticketMap.put("Todo", todoTickets)
            ticketMap.put("Doing", doingTickets)
            ticketMap.put("Done", doneTickets)
        } else {
            ticketMap.clear()
            ticketMap.put("Backlog", backlogTickets2)
            ticketMap.put("Todo", todoTickets2)
            ticketMap.put("Doing", doingTickets2)
            ticketMap.put("Done", doneTickets2)
        }
        return ticketMap[status]!!
    }

    fun createTicket(ticket: Ticket = ticketModel.item) {
        ticket.id = "8"
        backlogTickets.add(ticket)
    }

    fun deleteTicket(ticket: Ticket = ticketModel.item) {
        ticketMap[ticket.status]!!.remove(ticket)
    }

    fun updateTicketStatus(ticket: Ticket = ticketModel.item, status: String) {
        ticketMap[status]!!.add(ticketModel.item)
        ticketMap[ticket.status]!!.remove(ticketModel.item)
        ticket.status = status
    }

    fun createBoard(board: Board = boardModel.item) {
        board.id = "8"
        boards.add(board)
    }

    fun deleteBoard(board: Board = boardModel.item) {
        boards.remove(board)
    }


}